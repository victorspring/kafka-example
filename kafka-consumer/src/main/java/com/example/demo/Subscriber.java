package com.example.demo;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class Subscriber {

    @KafkaListener(topics = "${kafka.topic}", groupId = "default")
    public void consume(String message) {
        System.out.println("Message Received -> " + message);
    }
}
